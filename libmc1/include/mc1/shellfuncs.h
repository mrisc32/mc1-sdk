// -*- mode: c; tab-width: 2; indent-tabs-mode: nil; -*-
//--------------------------------------------------------------------------------------------------
// Copyright (c) 2024 Marcus Geelnard
//
// This software is provided 'as-is', without any express or implied warranty. In no event will the
// authors be held liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose, including commercial
// applications, and to alter it and redistribute it freely, subject to the following restrictions:
//
//  1. The origin of this software must not be misrepresented; you must not claim that you wrote
//     the original software. If you use this software in a product, an acknowledgment in the
//     product documentation would be appreciated but is not required.
//
//  2. Altered source versions must be plainly marked as such, and must not be misrepresented as
//     being the original software.
//
//  3. This notice may not be removed or altered from any source distribution.
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
// This interface gives access to the library routines that are provided by the shell.
//
// Example usage:
//
//   mc1_shell_base_t shell_base = mc1_shellfun_open();
//   if (shell_base) {
//     mc1_shellfun_putc_t putc =
//         (mc1_shellfun_putc_t)mc1_shellfun_get(shell_base, MC1_SHELLFN_PUTC_OFFS);
//     putc('A');
//   }
//--------------------------------------------------------------------------------------------------

#include <stdint.h>
#include <stddef.h>  // For NULL.

#ifndef MC1_SHELLFUNCS_H_
#define MC1_SHELLFUNCS_H_

#ifdef __cplusplus
extern "C" {
#endif

#define MC1_SHELLFN_BASE_ADDR_PTR 0x40000004U  // Fixed address for the pointer to the libray.
#define MC1_SHELLFN_MAGIC 0x8076af29U          // Magic ID (set if the library is available).

typedef void** mc1_shell_base_t;
typedef void (*mc1_shellfun_t)(void);

//--------------------------------------------------------------------------------------------------
// Available shell functions.
//--------------------------------------------------------------------------------------------------

#define MC1_SHELLFN_PUTC_OFFS 0
typedef void (*mc1_shellfun_putc_t)(const int);

//--------------------------------------------------------------------------------------------------
// Helpers.
//--------------------------------------------------------------------------------------------------

/// @brief Open the shell function library.
/// @returns The library base if successful, otherwise NULL.
static inline mc1_shell_base_t mc1_shellfun_open(void) {
  const uint32_t* lib_base = *(const uint32_t**)MC1_SHELLFN_BASE_ADDR_PTR;
  if (lib_base != NULL && *lib_base == MC1_SHELLFN_MAGIC) {
    return (mc1_shell_base_t)(&lib_base[1]);
  }
  return NULL;
}

/// @brief Get a shell function pointer.
/// @param lib_base Library base (must not be NULL).
/// @param offset Function offset.
/// @returns The function pointer.
static inline mc1_shellfun_t mc1_shellfun_get(mc1_shell_base_t lib_base, int offset) {
  void* fun_ptr = lib_base[offset];
  return (mc1_shellfun_t)fun_ptr;
}

#ifdef __cplusplus
}
#endif

#endif  // MC1_SHELLFUNCS_H_
