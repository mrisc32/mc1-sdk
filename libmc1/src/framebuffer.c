// -*- mode: c; tab-width: 2; indent-tabs-mode: nil; -*-
//--------------------------------------------------------------------------------------------------
// Copyright (c) 2020 Marcus Geelnard
//
// This software is provided 'as-is', without any express or implied warranty. In no event will the
// authors be held liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose, including commercial
// applications, and to alter it and redistribute it freely, subject to the following restrictions:
//
//  1. The origin of this software must not be misrepresented; you must not claim that you wrote
//     the original software. If you use this software in a product, an acknowledgment in the
//     product documentation would be appreciated but is not required.
//
//  2. Altered source versions must be plainly marked as such, and must not be misrepresented as
//     being the original software.
//
//  3. This notice may not be removed or altered from any source distribution.
//--------------------------------------------------------------------------------------------------

#include <mc1/framebuffer.h>

#include <mc1/memory.h>
#include <mc1/mmio.h>
#include <mc1/vcp.h>

#include <stdbool.h>
#include <string.h>

//--------------------------------------------------------------------------------------------------
// Private.
//--------------------------------------------------------------------------------------------------

static bool is_valid_mode(int mode) {
  switch (mode) {
    case CMODE_RGBA8888:
    case CMODE_RGBA5551:
    case CMODE_PAL8:
    case CMODE_PAL4:
    case CMODE_PAL2:
    case CMODE_PAL1:
    case CMODE_TEXT1:
    case CMODE_DXT1:
      return true;
    default:
      return false;
  }
}

static size_t bits_per_pixel(int mode) {
  switch (mode) {
    case CMODE_RGBA8888:
      return 32;
    case CMODE_RGBA5551:
      return 16;
    case CMODE_PAL8:
    case CMODE_TEXT1:
      return 8;
    case CMODE_PAL4:
    case CMODE_DXT1:
      return 4;
    case CMODE_PAL2:
      return 2;
    case CMODE_PAL1:
      return 1;
    default:
      return 0;
  }
}

static size_t palette_entries(int mode) {
  switch (mode) {
    case CMODE_PAL8:
    case CMODE_TEXT1:
      return 256;
    case CMODE_PAL4:
      return 16;
    case CMODE_PAL2:
      return 4;
    case CMODE_PAL1:
      return 2;
    default:
      return 0;
  }
}

static size_t calc_stride(int width, int mode) {
  if (mode == CMODE_DXT1) {
    // The width must be a multiple of four, the stride includes four rows, and 4x4 pixels are
    // stored in 64 bits (4 bpp).
    const size_t pixels_per_row = 4U * ((((size_t)width) + 3U) / 4U);
    return pixels_per_row * 2U;
  }

  // We round up to the nearest word size.
  return 4U * ((((size_t)width * bits_per_pixel(mode)) + 31U) / 32U);
}

static size_t calc_pixels_size(int width, int height, int mode) {
  if (mode == CMODE_DXT1) {
    // For DXT1 the stride represents four rows.
    return calc_stride(width, mode) * ((((size_t)height) + 3U) / 4U);
  }
  return calc_stride(width, mode) * (size_t)height;
}

static size_t calc_vcp_size(int height, int mode) {
  // Prologue: set XINCR and CMODE.
  size_t prologue_words = 2;
  if (mode == CMODE_TEXT1) {
    // We set the two text colors as part of the prologue.
    prologue_words += 2;
  }

  size_t palette_words = palette_entries(mode);
  if (palette_words > 0u) {
    // We do SETPAL in the prologue.
    ++prologue_words;
  }

  // We always need to set HSTOP once per frame.
  size_t row_words = 1;
  if (mode == CMODE_TEXT1) {
    // In text mode we:
    //   Row 8*N:   WAIT + set SUBROW + set ADDR
    //   Row 8*N+1: WAIT + set SUBROW
    //   Row 8*N+2: WAIT + set SUBROW
    //   ...
    //   Row 8*N+7: WAIT + set SUBROW
    row_words += (8 * 2 + 1) * height;
  } else if (mode == CMODE_DXT1) {
    // In DXT1 mode we:
    //   Row 4*N:   WAIT + set SUBROW + set ADDR
    //   Row 4*N+1: WAIT + set SUBROW
    //   Row 4*N+2: WAIT + set SUBROW
    //   Row 4*N+3: WAIT + set SUBROW
    row_words += (4 * 2 + 1) * height;
  } else {
    // In normal modes we WAIT + set ADDR on every pixel row.
    row_words += 2 * height;
  }

  // WAIT forever.
  size_t epilogue_words = 1;

  return (prologue_words + palette_words + row_words + epilogue_words) * 4;
}

static int subrows_per_addr_row_for_mode(int mode) {
  if (mode == CMODE_TEXT1) {
    return 8;
  }
  if (mode == CMODE_DXT1) {
    return 4;
  }
  return 1;
}

static int addr_rows_for_mode(int height, int mode) {
  if (mode == CMODE_DXT1) {
    return (int)(((unsigned)height + 3U) / 4U);
  }
  // Note: In text mode the height is given in number of characters (not pixel rows).
  return height;
}

//--------------------------------------------------------------------------------------------------
// Public.
//--------------------------------------------------------------------------------------------------

fb_t* fb_create(int width, int height, int mode) {
  // Sanity check input parameters.
  if (width < 1 || height < 1 || !is_valid_mode(mode)) {
    return NULL;
  }

  // Allocate memory for the framebuffer and supporting data structures.
  const size_t vcp_size = calc_vcp_size(height, mode);
  const size_t pix_size = calc_pixels_size(width, height, mode);
  const size_t total_size = sizeof(fb_t) + vcp_size + pix_size;
  fb_t* fb = (fb_t*)vmem_alloc(total_size);
  if (!fb) {
    return NULL;
  }
  memset(fb, 0, total_size);

  // Populate the fb_t object fields.
  {
    uint8_t* ptr = (uint8_t*)fb;
    fb->vcp = (uint32_t*)&ptr[sizeof(fb_t)];
    fb->pixels = (uint32_t*)&ptr[sizeof(fb_t) + vcp_size];
  }
  fb->stride = calc_stride(width, mode);
  fb->width = width;
  fb->height = height;
  fb->mode = mode;

  // Get the native width and height of the video signal.
  const uint32_t native_width = MMIO(VIDWIDTH);
  const uint32_t native_height = MMIO(VIDHEIGHT);

  uint32_t* vcp = fb->vcp;

  // VCP prologue.
  *vcp++ = vcp_emit_setreg(VCR_XINCR, (0x010000 * width) / native_width);
  *vcp++ = vcp_emit_setreg(VCR_CMODE, mode);
  if (mode == CMODE_TEXT1) {
    fb->textcolors = vcp;
    *vcp++ = vcp_emit_setreg(VCR_TEXTBG, 0xe00040);
    *vcp++ = vcp_emit_setreg(VCR_TEXTFG, 0xfff7fe);
  }

  // Palette.
  size_t pal_N = palette_entries(mode);
  if (pal_N > 0u) {
    *vcp++ = vcp_emit_setpal(0, pal_N);
    fb->palette = (void*)vcp;
    for (uint32_t k = 0; k < pal_N; ++k) {
      *vcp++ = ((k * 255u) / pal_N) * 0x01010101u;
    }
  }

  // Address pointers and subrow indices.
  const uint32_t vcp_fb_stride = fb->stride / 4u;
  const int subrows_per_addr_row = subrows_per_addr_row_for_mode(mode);
  const int addr_rows = addr_rows_for_mode(height, mode);
  const int pixel_height = subrows_per_addr_row * addr_rows;
  uint32_t vcp_fb_addr = to_vcp_addr((uintptr_t)fb->pixels);
  uint32_t pixel_row = 0;
  for (int k = 0; k < addr_rows; ++k) {
    for (int subrow = 0; subrow < subrows_per_addr_row; ++subrow) {
      uint32_t y = (pixel_row * native_height) / (uint32_t)pixel_height;
      *vcp++ = vcp_emit_waity(y);
      if (y == 0) {
        *vcp++ = vcp_emit_setreg(VCR_HSTOP, native_width);
      }
      if (subrow == 0) {
        *vcp++ = vcp_emit_setreg(VCR_ADDR, vcp_fb_addr);
      }
      if (subrows_per_addr_row > 1) {
        *vcp++ = vcp_emit_setreg(VCR_SUBROW, subrow);
      }
      ++pixel_row;
    }
    vcp_fb_addr += vcp_fb_stride;
  }

  // Wait forever.
  *vcp++ = vcp_emit_waity(32767);

  return fb;
}

void fb_destroy(fb_t* fb) {
  vmem_free(fb);
}

void fb_show(fb_t* fb, layer_t layer) {
  if (fb != NULL) {
    vcp_set_prg(layer, fb->vcp);
  }
}

