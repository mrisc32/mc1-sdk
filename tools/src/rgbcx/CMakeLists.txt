# -*- mode: CMake; tab-width: 4; indent-tabs-mode: nil; -*-

set(SOURCES
    rgbcx_c.cpp
    rgbcx_c.h
    rgbcx.cpp
    rgbcx.h
    rgbcx_table4.h
    rgbcx_table4_small.h
    )
add_library(rgbcx ${SOURCES})
target_include_directories(rgbcx PUBLIC .)
set_property(TARGET rgbcx PROPERTY CXX_STANDARD 11)
set_property(TARGET rgbcx PROPERTY CXX_EXTENSIONS OFF)

