// -*- mode: c; tab-width: 2; indent-tabs-mode: nil; -*-
//--------------------------------------------------------------------------------------------------
// Copyright (c) 2024 Marcus Geelnard
//
// This software is provided 'as-is', without any express or implied warranty. In no event will the
// authors be held liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose, including commercial
// applications, and to alter it and redistribute it freely, subject to the following restrictions:
//
//  1. The origin of this software must not be misrepresented; you must not claim that you wrote
//     the original software. If you use this software in a product, an acknowledgment in the
//     product documentation would be appreciated but is not required.
//
//  2. Altered source versions must be plainly marked as such, and must not be misrepresented as
//     being the original software.
//
//  3. This notice may not be removed or altered from any source distribution.
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
// A C interface to rgbcx
//--------------------------------------------------------------------------------------------------

#ifndef RGBCX_C_H_
#define RGBCX_C_H_

#include <stdbool.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

// BC1 approximation mode (passed to rgbcx_init).
#define RGBCX_BC1_APPROX_IDEAL 0
#define RGBCX_BC1_APPROX_NVIDIA 1
#define RGBCX_BC1_APPROX_AMD 2
#define RGBCX_BC1_APPROX_IDEAL_ROUND4 3

// BC1 compression level range.
#define RGBCX_MIN_LEVEL 0
#define RGBCX_MAX_LEVEL 18

// rgbcx_init() MUST be called once before using the BC1 encoder.
void rgbcx_init(int mode);

// Encodes a 4x4 block of RGBX (X=ignored) pixels to BC1 format.
// This is the simplified interface for BC1 encoding, which accepts a level parameter and converts
// that to the best overall flags. The pixels are in RGBA format, where R is first in memory. The
// BC1 encoder completely ignores the alpha channel (i.e. there is no punchthrough alpha support).
// This is the recommended function to use for BC1 encoding, becuase it configures the encoder for
// you in the best possible way (on average). Note that the 3 color modes won't be used at all until
// level 5 or higher. No transparency supported, however if you set use_transparent_texels_for_black
// to true the encocer will use transparent selectors on very dark/black texels to reduce MSE.
void rgbcx_encode_bc1(uint32_t level,
                      void* pDst,
                      const uint8_t* pPixels,
                      bool allow_3color,
                      bool use_transparent_texels_for_black);

#ifdef __cplusplus
}
#endif

#endif  // RGBCX_C_H_
