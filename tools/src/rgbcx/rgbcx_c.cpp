// -*- mode: c++; tab-width: 2; indent-tabs-mode: nil; -*-
//--------------------------------------------------------------------------------------------------
// Copyright (c) 2024 Marcus Geelnard
//
// This software is provided 'as-is', without any express or implied warranty. In no event will the
// authors be held liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose, including commercial
// applications, and to alter it and redistribute it freely, subject to the following restrictions:
//
//  1. The origin of this software must not be misrepresented; you must not claim that you wrote
//     the original software. If you use this software in a product, an acknowledgment in the
//     product documentation would be appreciated but is not required.
//
//  2. Altered source versions must be plainly marked as such, and must not be misrepresented as
//     being the original software.
//
//  3. This notice may not be removed or altered from any source distribution.
//--------------------------------------------------------------------------------------------------

#include "rgbcx_c.h"

#include "rgbcx.h"

extern "C" void rgbcx_init(int mode) {
  rgbcx::init(static_cast<rgbcx::bc1_approx_mode>(mode));
}

extern "C" void rgbcx_encode_bc1(uint32_t level,
                                 void* pDst,
                                 const uint8_t* pPixels,
                                 bool allow_3color,
                                 bool use_transparent_texels_for_black) {
  rgbcx::encode_bc1(level, pDst, pPixels, allow_3color, use_transparent_texels_for_black);
}
